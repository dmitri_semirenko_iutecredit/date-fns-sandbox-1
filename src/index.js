import tag from './tag';
import { parseISO } from 'date-fns';
import { zonedTimeToUtc, utcToZonedTime, format } from 'date-fns-tz';

// https://en.wikipedia.org/wiki/List_of_tz_database_time_zones
const timeZone = 'America/Guadeloupe'; //'Europe/Berlin'; // 'America/New_York';//
//const pattern = 'dd.MM.yyyy HH:mm:ss.SSS \'GMT\' XXX (z)'
const pattern = 'dd.MM.yyyy HH:mm:ss.SSS OOOO'

const testCases = {
    'a': '2021-07-09T09:00:00+03:00',
    'b': '2021-07-09T06:00:00Z',
    'c': '2021-07-09T05:00:00-01:00'
};

const trEl = tag('tr')
const tdEl = tag('td', 'border: 1px solid black;')
const tableEl = tag('table', 'width: 80%; border: 1px solid black;')
const rows = [];
rows.push(
    trEl(
        tdEl('initial key'),
        tdEl('initial value'),
        tdEl('parsedValue'),
        tdEl('parsedValue.toIsoString'),
        tdEl('format'),
        tdEl('utcToZonedTime')
    )
);
for (const key in testCases) {
    const value = testCases[key];
    const parsedValue = parseISO(value);
    rows.push(trEl(
        tdEl(key),
        tdEl(value),
        tdEl(parsedValue),
        tdEl(parsedValue.toISOString()),
        tdEl(format(parsedValue, pattern, { timeZone })),
        tdEl(utcToZonedTime(parsedValue.toISOString(), timeZone ))
    ));
}

const rootEl = document.getElementById('root');
rootEl.appendChild(tableEl(...rows));
